extends Control

onready var menu : Control = $CenterContainer/Menu

onready var active : bool = false

signal resume
signal restart_level
signal back_to_title

func show_pause_menu() -> void:
	menu.current_child = 0
	menu.move_cursor()
	menu.active = true
	show()

func resume() -> void:
	menu.active = false
	hide()
	emit_signal("resume")

func restart_level() -> void:
	menu.active = false
	hide()
	emit_signal("restart_level")

func back_to_title() -> void:
	menu.active = false
	hide()
	emit_signal("back_to_title")

func _on_Menu_button_pressed(slug : String) -> void:
	match slug:
		"resume":
			resume()
		"restart":
			restart_level()
		"back_to_title":
			back_to_title()

func _on_Menu_back_from_root() -> void:
	resume()

func _ready() -> void:
	menu.items = {
		"pause_menu": {
			"type": "menu",
			"children": ["resume", "restart", "settings", "back_to_title"]
		},
		"resume": {
			"type": "button",
			"label": "Resume"
		},
		"restart": {
			"type": "button",
			"label": "Restart Level"
		},
		"settings": {
			"label": "Settings",
			"type": "menu",
			"children": ["video", "audio", "controls", "back"],
			"parent": "pause_menu"
		},
		"video": {
			"label": "Video",
			"type": "menu",
			"children": ["fullscreen", "show_cursor", "back"],
			"parent": "settings"
		},
		"fullscreen": {
			"label": "Fullscreen",
			"type": "variable",
			"variable_name": "fullscreen"
		},
		"show_cursor": {
			"label": "Show Mouse Cursor",
			"type": "variable",
			"variable_name": "show_cursor"
		},
		"audio": {
			"label": "Audio",
			"type": "menu",
			"children": ["sfx_volume", "bgm_volume", "back"],
			"parent": "settings"
		},
		"sfx_volume": {
			"label": "SFX",
			"type": "variable",
			"variable_name": "sfx_volume"
		},
		"bgm_volume": {
			"label": "Music",
			"type": "variable",
			"variable_name": "bgm_volume"
		},
		"controls": {
			"label": "Controls",
			"type": "menu",
			"children": ["key_binding_run_left", "key_binding_run_right", "key_binding_jump", "key_binding_interact", "key_binding_restart", "key_binding_pause", "back"],
			"parent": "settings"
		},
		"key_binding_run_left": {
			"label": "Run Left",
			"type": "key_binding",
			"action_name": "run_left"
		},
		"key_binding_run_right": {
			"label": "Run Right",
			"type": "key_binding",
			"action_name": "run_right"
		},
		"key_binding_jump": {
			"label": "Jump",
			"type": "key_binding",
			"action_name": "jump"
		},
		"key_binding_interact": {
			"label": "Interact",
			"type": "key_binding",
			"action_name": "interact"
		},
		"key_binding_restart": {
			"label": "Restart Level",
			"type": "key_binding",
			"action_name": "restart"
		},
		"key_binding_pause": {
			"label": "Pause",
			"type": "key_binding",
			"action_name": "pause"
		},
		"back_to_title": {
			"type": "button",
			"label": "Back to Title Screen"
		},
		"back": {
			"type": "button",
			"label": "Back"
		}
	}
	menu.variables = {
		"fullscreen": {"type": "tickbox", "value": Settings.fullscreen},
		"show_cursor": {"type": "tickbox", "value": Settings.show_cursor},
		"sfx_volume": {"type": "volume", "value": Settings.sfx_volume},
		"bgm_volume": {"type": "volume", "value": Settings.bgm_volume}
	}
	menu.current_item = "pause_menu"
	menu.resize(true)
	menu.active = false
	menu.connect("back_from_root", self, "resume")

func _on_Menu_variable_changed(variable_name : String) -> void:
	Settings.fullscreen = menu.variables["fullscreen"]["value"]
	Settings.show_cursor = menu.variables["show_cursor"]["value"]
	Settings.sfx_volume = menu.variables["sfx_volume"]["value"]
	Settings.bgm_volume = menu.variables["bgm_volume"]["value"]
	Settings.apply_config()
	Settings.save_config()
