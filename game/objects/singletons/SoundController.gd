extends Node

var sounds : Dictionary
var groups : Dictionary

func play_sound_group(name : String) -> void:
	var choices : Array = groups[name]
	var which : int = randi() % choices.size()
	var audio_player : AudioStreamPlayer = sounds[choices[which]]
	audio_player.play()

func play_sound(name : String) -> void:
	# Is this the name of a sound group?
	if name in groups:
		play_sound_group(name)
	elif name in sounds:
		var audio_player : AudioStreamPlayer = sounds[name]
		audio_player.play()
	else:
		printerr("%s is not a known sound" % name)

func load_sound_files(sound_data : Array) -> void:
	sounds = {}
	for sound in sound_data:
		var filename : String = sound["filename"]
		var path : String = "res://sounds/%s.ogg" % filename
		var stream : AudioStreamOGGVorbis = load(path)
		var audio_player : AudioStreamPlayer = AudioStreamPlayer.new()
		audio_player.stream = stream
		audio_player.bus = "SFX"
		audio_player.volume_db = sound["volume"]
		add_child(audio_player)
		sounds[filename] = audio_player
		print("Loaded %s" % path)

func _ready() -> void:
	var data : Dictionary = Utilities.load_data_from_json("res://data/sounds.json")
	groups = data["groups"]
	load_sound_files(data["sounds"])
