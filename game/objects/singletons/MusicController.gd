extends Node

const MUSIC_NAMES : Array = [
	"Ice Dungeon",
	"Night Watch",
	"Pause Menu Theme",
	"Photographs",
	"Stage 02",
	"Stygiomedusa Gigantea",
	"Take My Hand",
	"To The West",
	"Wake Up"
]

var tracks : Dictionary

onready var audio_player : AudioStreamPlayer = $AudioStreamPlayer
onready var tween : Tween = $Tween

var current_track : String
onready var currently_playing : bool = false

func play_music(name : String, fade_out_last : bool = true, fade_out_time : float = 1.0) -> void:
	if currently_playing and current_track == name:
		return
	if fade_out_last and currently_playing:
		tween.interpolate_property(audio_player, "volume_db", audio_player.volume_db, linear2db(0.1), fade_out_time, Tween.TRANS_SINE, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_all_completed")
	audio_player.stop()
	audio_player.stream = tracks[name]
	audio_player.volume_db = linear2db(1.0)
	audio_player.play()
	currently_playing = true
	current_track = name

func fade_out_music(duration : float) -> void:
	currently_playing = false
	tween.interpolate_property(audio_player, "volume_db", audio_player.volume_db, linear2db(0.01), duration, Tween.TRANS_SINE, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")
	audio_player.stop()

func stop_music() -> void:
	currently_playing = false
	audio_player.stop()

func load_music(name : String) -> AudioStreamOGGVorbis:
	var path : String = "res://music/%s.ogg" % name
	var stream : AudioStreamOGGVorbis = load(path)
	print("Loaded %s" % path)
	return stream

func load_all_tracks() -> void:
	tracks = {}
	for music_name in MUSIC_NAMES:
		var stream : AudioStreamOGGVorbis = load_music(music_name)
		tracks[music_name] = stream

func _ready() -> void:
	load_all_tracks()
