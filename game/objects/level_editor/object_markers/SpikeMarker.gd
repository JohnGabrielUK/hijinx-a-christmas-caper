extends Sprite

class_name SpikeMarker

var board_position : Vector2

func get_hints() -> String:
	return "(Z) Change sprite"

func change_property() -> void:
	frame = wrapi(frame + 1, 0, hframes)

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_SPIKE,
		"position": [board_position.x, board_position.y],
		"frame": frame
	}
