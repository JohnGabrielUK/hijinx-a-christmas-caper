extends Sprite

class_name HammerMarker

var board_position : Vector2
onready var starting_delay : float = 1.0

func get_hints() -> String:
	return "(Z) Change starting delay (currently %.1f s)" % starting_delay

func change_property() -> void:
	starting_delay = wrapf(starting_delay + 0.5, 0.0, 5.5)

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_HAMMER,
		"position": [board_position.x, board_position.y],
		"starting_delay": starting_delay
	}
