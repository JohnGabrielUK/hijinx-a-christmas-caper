extends Sprite

class_name CarlMarker

var board_position : Vector2
onready var which_conversation : String = "test1"

func get_hints() -> String:
	return "(Z) Conversation = %s" % which_conversation

func change_property() -> void:
	var conversations : Array = Conversations.list_conversations()
	var index : int = wrapi(conversations.find(which_conversation) + 1, 0, conversations.size())
	which_conversation = conversations[index]

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_CARL,
		"position": [board_position.x, board_position.y],
		"conversation": which_conversation
	}
