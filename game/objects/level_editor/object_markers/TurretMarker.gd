extends Sprite

class_name TargetMarker

const DIRECTIONS : Array = [Vector2.RIGHT, Vector2(1, 1), Vector2.DOWN, Vector2(-1, 1), Vector2.LEFT, Vector2(-1, -1), Vector2.UP, Vector2(1, -1)]

const GUN_DIRECTIONS : Dictionary = {
	Vector2.LEFT: {"rotation": 0.0, "flip_h": true, "flip_v": false, "frame": 0},
	Vector2.RIGHT: {"rotation": 0.0, "flip_h": false, "flip_v": false, "frame": 0},
	Vector2.UP: {"rotation": 90.0, "flip_h": true, "flip_v": true, "frame": 0},
	Vector2.DOWN: {"rotation": 90.0, "flip_h": false, "flip_v": true, "frame": 0},
	Vector2(1, 1): {"rotation": 0.0, "flip_h": false, "flip_v": false, "frame": 1},
	Vector2(-1, 1): {"rotation": 0.0, "flip_h": true, "flip_v": false, "frame": 1},
	Vector2(-1, -1): {"rotation": -90.0, "flip_h": false, "flip_v": true, "frame": 1},
	Vector2(1, -1): {"rotation": -90.0, "flip_h": false, "flip_v": false, "frame": 1}
}

onready var sprite_gun : Sprite = $Sprite_Gun

var board_position : Vector2
onready var firing_direction : Vector2 = Vector2.RIGHT
onready var firing_interval : float = 1.0

func get_hints() -> String:
	return "(Z) Change firing direction\n(X) Change firing interval (currently %.1f s)" % firing_interval

func set_firing_direction(direction : Vector2) -> void:
	firing_direction = direction
	var direction_properties : Dictionary = GUN_DIRECTIONS[direction]
	sprite_gun.rotation_degrees = direction_properties["rotation"]
	sprite_gun.flip_h = direction_properties["flip_h"]
	sprite_gun.flip_v = direction_properties["flip_v"]
	sprite_gun.frame = direction_properties["frame"]

func set_firing_interval(interval : float) -> void:
	firing_interval = interval

func change_property() -> void:
	var current_index : int = DIRECTIONS.find(firing_direction)
	var next_index : int = wrapi(current_index + 1, 0, DIRECTIONS.size())
	set_firing_direction(DIRECTIONS[next_index])

func change_property_alt() -> void:
	firing_interval = wrapf(firing_interval + 0.5, 0.5, 2.5)

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_TURRET,
		"position": [board_position.x, board_position.y],
		"firing_direction": [firing_direction.x, firing_direction.y],
		"firing_interval": firing_interval
	}

