extends Sprite

enum TYPE {LAND_DUST, RUN_DUST, SLIDE_DUST, SPARKLE, BRICK, DUST, RED_SPARKLE, YELLOW_SPARKLE, GREEN_SPARKLE, BLUE_SPARKLE, GUNFLASH}

const TYPE_DEFS : Dictionary = {
	TYPE.LAND_DUST: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/land_dust.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 0),
		"max_speed": 512.0
	},
	TYPE.RUN_DUST: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/run_dust.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 0),
		"max_speed": 512.0
	},
	TYPE.SLIDE_DUST: {
		"hframes": 8,
		"vframes": 1,
		"texture": preload("res://sprites/particles/slide_dust.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 0),
		"max_speed": 512.0
	},
	TYPE.SPARKLE: {
		"hframes": 4,
		"vframes": 1,
		"texture": preload("res://sprites/particles/sparkle.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 0),
		"max_speed": 512.0
	},
	TYPE.BRICK: {
		"hframes": 8,
		"vframes": 1,
		"texture": preload("res://sprites/particles/brick.png"),
		"frame_interval": 0.1,
		"accel": Vector2(0, 512),
		"max_speed": 512.0
	},
	TYPE.DUST: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/dust.png"),
		"frame_interval": 0.1,
		"accel": Vector2(0, 512),
		"max_speed": 512.0
	},
	TYPE.RED_SPARKLE: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/red_sparkle.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 128),
		"max_speed": 128.0
	},
	TYPE.YELLOW_SPARKLE: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/yellow_sparkle.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 128),
		"max_speed": 128.0
	},
	TYPE.GREEN_SPARKLE: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/green_sparkle.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 128),
		"max_speed": 128.0
	},
	TYPE.BLUE_SPARKLE: {
		"hframes": 6,
		"vframes": 1,
		"texture": preload("res://sprites/particles/blue_sparkle.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 128),
		"max_speed": 128.0
	},
	TYPE.GUNFLASH: {
		"hframes": 4,
		"vframes": 1,
		"texture": preload("res://sprites/particles/gunflash.png"),
		"frame_interval": 0.05,
		"accel": Vector2(0, 0),
		"max_speed": 128.0
	}
}

onready var timer_next_frame : Timer = $Timer_NextFrame

var velocity : Vector2
var accel : Vector2
var max_speed : float
onready var no_accel : bool = false

func set_particle_type(type : int) -> void:
	var data : Dictionary = TYPE_DEFS[type]
	texture = data["texture"]
	hframes = data["hframes"]
	vframes = data["vframes"]
	accel = data["accel"]
	max_speed = data["max_speed"]
	timer_next_frame.wait_time = data["frame_interval"]
	timer_next_frame.start()

func _process(delta : float) -> void:
	if not no_accel:
		velocity.x = clamp(velocity.x + (accel.x * delta), -max_speed, max_speed)
		velocity.y = clamp(velocity.y + (accel.y * delta), -max_speed, max_speed)
	position += velocity * delta

func _on_Timer_NextFrame_timeout() -> void:
	if frame + 1 >= (hframes * vframes):
		queue_free()
	else:
		frame += 1
