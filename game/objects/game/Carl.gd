extends KinematicBody2D

class_name Carl

onready var area_interact : Area2D = $Area2D_Interact
onready var camera : Camera2D = $Camera2D

var which_conversation : String
onready var auto_talk : bool = false

signal conversation_started

func can_interact(body : PhysicsBody2D) -> bool:
	return area_interact.overlaps_body(body)

func interact() -> void:
	emit_signal("conversation_started", which_conversation)

func _process(delta : float) -> void:
	if auto_talk:
		camera.current = true
		interact()
		auto_talk = false
