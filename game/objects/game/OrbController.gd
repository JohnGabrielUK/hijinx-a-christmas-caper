extends Node

const ORB_SOUNDS : Array = ["orb_bell1", "orb_bell5", "orb_bell8", "orb_bell11"]

var goal : Node2D

onready var orbs_touched : int = 0
onready var already_done : bool = false

func reveal_goal() -> void:
	SoundController.play_sound("orb_goal")
	already_done = true
	get_tree().call_group("orb", "release_energy_in_sequence")
	yield(get_tree().create_timer(4.0), "timeout")
	get_tree().call_group("orb_energy", "move_on_target", goal.global_position)
	yield(get_tree().create_timer(0.5), "timeout")
	goal.reveal()

func orb_touched(which : int) -> void:
	if already_done:
		return
	if which == orbs_touched:
		SoundController.play_sound(ORB_SOUNDS[orbs_touched])
		orbs_touched += 1
		get_tree().call_group("orb", "make_ready", orbs_touched)
		if orbs_touched == 4:
			get_tree().call_group("orb_sparkles", "queue_free")
			reveal_goal()
	else:
		SoundController.play_sound("orb_error")

func _ready() -> void:
	get_tree().call_group("orb", "make_ready", 0)
