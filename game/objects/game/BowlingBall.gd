extends KinematicBody2D

const MAX_FALL_SPEED : float = 512.0
const FALL_INCR : float = 512.0
const BOUNCE_SPEED : float = 256.0

onready var velocity : Vector2 = Vector2(144, 0)

func _process(delta : float) -> void:
	velocity.y = clamp(velocity.y + (FALL_INCR * delta), -MAX_FALL_SPEED, MAX_FALL_SPEED)
	var collision : KinematicCollision2D = move_and_collide(velocity * delta)
	if collision != null:
		# If it's the player, ouchies
		if collision.collider is Player:
			collision.collider.get_crushed()
		# Else, bouncy bouncy
		else:
			SoundController.play_sound("bowling_ball_bounce")
			if collision.normal == Vector2.DOWN:
				velocity.y = clamp(velocity.y, 0, MAX_FALL_SPEED)
			elif collision.normal in [Vector2.LEFT, Vector2.RIGHT]:
				velocity.x *= -1
			else:
				velocity.y = -BOUNCE_SPEED
