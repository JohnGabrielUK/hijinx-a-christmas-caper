extends Area2D

const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

onready var sprite : Sprite = $Sprite

var anim_index : float = 0

signal ring_collected

func _process(delta : float) -> void:
	anim_index += delta
	sprite.frame = wrapi(anim_index * 10.0, 0, sprite.hframes)
	sprite.offset.y = sin(anim_index * 4.0) * 2.0

func _body_entered(body : Node) -> void:
	if body is Player:
		var particle : Sprite = _Particle.instance()
		particle.global_position = global_position
		particle.velocity = Vector2.UP * 32.0
		get_parent().add_child(particle)
		particle.set_particle_type(particle.TYPE.SPARKLE)
		SoundController.play_sound("ring_collect")
		emit_signal("ring_collected")
		queue_free()
