extends Area2D

onready var sprite : Sprite = $Sprite

signal goal_reached

var anim_index : float = 0
var hidden : bool

func make_hidden() -> void:
	hidden = true
	modulate.a = 0.0

func reveal() -> void:
	hidden = false
	modulate.a = 1.0

func _process(delta : float) -> void:
	anim_index += delta
	sprite.frame = wrapi(anim_index * 10.0, 0, sprite.hframes)
	sprite.offset.y = sin(anim_index * 4.0) * 2.0

func _body_entered(body : Node) -> void:
	if body is Player and not hidden:
		emit_signal("goal_reached")
