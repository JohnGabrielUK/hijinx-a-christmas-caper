extends Node

static func get_map_size(geometry : TileMap) -> Vector2:
	return geometry.get_used_rect().end

static func get_geometry_array(map_size : Vector2, geometry : Node) -> Array:
	var geometry_array : Array = []
	for y in range(0, map_size.y):
		for x in range(0, map_size.x):
			var pos : Vector2 = Vector2(x, y)
			var tile_type : int = geometry.get_tile_type_at(pos)
			match tile_type:
				Constants.TILEMAP_DIRT:
					geometry_array.append(Constants.LEVELTILE_DIRT)
				Constants.TILEMAP_ICE:
					geometry_array.append(Constants.LEVELTILE_ICE)
				Constants.TILEMAP_BRICK:
					geometry_array.append(Constants.LEVELTILE_BRICK)
				_:
					geometry_array.append(Constants.LEVELTILE_EMPTY)
	return geometry_array

static func save_room(geometry : TileMap, player_spawn_position : Vector2) -> Dictionary:
	var map_size : Vector2 = get_map_size(geometry)
	var geometry_array : Array = get_geometry_array(map_size, geometry)
	var geometry_byte_array : PoolByteArray = PoolByteArray(geometry_array)
	var geometry_byte_array_length : int = geometry_byte_array.size()
	geometry_byte_array = geometry_byte_array.compress(Constants.COMPRESSION_TYPE)
	var geometry_base64 : String = Marshalls.variant_to_base64(geometry_byte_array)
	return {
		"level_format_version": Constants.LEVEL_FORMAT_VERSION,
		"geometry": geometry_base64,
		"geometry_width": int(map_size.x),
		"geometry_length": geometry_byte_array_length,
		"player_spawn_position": [player_spawn_position.x, player_spawn_position.y],
		"objects": geometry.get_objects(),
		"flags": geometry.get_flags()
	}
