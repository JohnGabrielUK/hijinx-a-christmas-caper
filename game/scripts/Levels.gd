extends Node

onready var levels : Dictionary = Utilities.load_data_from_json("res://data/levels.json")
onready var sets : Dictionary = Utilities.load_data_from_json("res://data/sets.json")

func get_number_of_level_sets() -> int:
	return sets.size()

func get_level_set_name_by_index(index : int) -> String:
	return sets.keys()[index]

func get_level_set_title(set_name : String) -> String:
	if not set_name in sets:
		printerr("%s not a recognised level set" % set_name)
	return sets[set_name]["title"]

func get_level_set_subtitle(set_name : String) -> String:
	if not set_name in sets:
		printerr("%s not a recognised level set" % set_name)
	return sets[set_name]["subtitle"]

func get_levels_in_set(set_name : String) -> Array:
	if not set_name in sets:
		printerr("%s not a recognised level set" % set_name)
	return sets[set_name]["levels"]

func get_epilogue_for_set(set_name : String) -> String:
	if not set_name in sets:
		printerr("%s not a recognised level set" % set_name)
	return sets[set_name]["epilogue"]

func get_next_set(set_name : String) -> String:
	if not set_name in sets:
		printerr("%s not a recognised level set" % set_name)
	var index : int = sets.keys().find(set_name)
	# If this is the last set in the game, then there's nothing to more onto
	if index >= get_number_of_level_sets() - 1:
		return ""
	else:
		return get_level_set_name_by_index(index + 1)

func get_level_in_set(set_name : String, which_level : int) -> String:
	return get_levels_in_set(set_name)[which_level]

func get_number_of_levels_in_set(set_name : String) -> int:
	return get_levels_in_set(set_name).size()

func get_level_title(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["title"]

func get_level_subtitle(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["subtitle"]

func get_level_par(level_name : String) -> int:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["par"]

func get_level_dev_best(level_name : String) -> int:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["devs_best"]

func get_music_for_level(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["bgm_track"]

func get_bg_palette_for_level(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["bg_palette"]

func get_bg_mountains_for_level(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["bg_mountains"]

func get_level_path(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	return levels[level_name]["path"]

func get_level_next(level_name : String) -> String:
	if not level_name in levels:
		printerr("%s not a recognised level" % level_name)
	# Try to find it in the sets
	for set_name in sets:
		var set_levels : Array = get_levels_in_set(set_name)
		if set_levels.has(level_name):
			var position : int = set_levels.find(level_name)
			if position >= get_number_of_levels_in_set(set_name) - 1:
				return get_epilogue_for_set(set_name)
			else:
				return get_level_in_set(set_name, position + 1)
	# We couldn't find it. Panic!
	printerr("Couldn't find next level for " % level_name)
	return ""

func get_level_data(name : String) -> Dictionary:
	var path : String = get_level_path(name)
	return Utilities.load_data_from_json(path)
