extends Control

# This entire script is jank don't look

onready var label_a : Label = $LabelA
onready var label_b : Label = $LabelB
onready var audio : AudioStreamPlayer = $AudioStreamPlayer
onready var tween : Tween = $Tween

func _ready() -> void:
	get_tree().paused = false
	if GameSession.selected_set == "b":
		label_a.text = "Thank You For Playing!"
		label_b.text = "Carl and Jinx will return..."
	tween.interpolate_property(label_a, "modulate", Color.transparent, Color.white, 0.25)
	tween.interpolate_property(label_b, "modulate", Color.transparent, Color.white, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 1.0)
	tween.interpolate_property(label_a, "modulate", Color.white, Color.transparent, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 3.0)
	tween.interpolate_property(label_b, "modulate", Color.white, Color.transparent, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 3.0)
	yield(get_tree().create_timer(1.0), "timeout")
	tween.start()
	if GameSession.selected_set == "a":
		audio.play()
	yield(tween, "tween_all_completed")
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _enter_tree() -> void:
	$LabelA.modulate = Color.transparent
	$LabelB.modulate = Color.transparent
