extends Node2D

const _GameSection : PackedScene = preload("res://objects/game/GameSection.tscn")

onready var achievement_popup : Control = $Overlay/AchievementPopup
onready var pause_screen : Control = $Overlay/PauseScreen
onready var dialogue_window : Control = $Overlay/DialogueWindow
onready var level_clear : Control = $Overlay/LevelClear
onready var cutout : ColorRect = $Overlay/Cutout

var game_section : Node2D

onready var current_set : String = GameSession.selected_set
onready var current_level : int = GameSession.selected_level
onready var paused : bool = false
onready var in_game : bool = false # So we can't pause during level changes et al
onready var in_convo : bool = false

func restart_level() -> void:
	init_game_section(Levels.get_level_in_set(current_set, current_level))
	paused = false
	get_tree().paused = false

func center_cutout_on_player() -> void:
	var player_pos : Vector2 = game_section.geometry.player.position
	player_pos -= Vector2(16, 16)
	player_pos /= Vector2(640, 360)
	cutout.set_center(player_pos)

func record_level_cleared(level_name : String, clear_time : int, got_rings : int) -> void:
	GameProgress.set_level_cleared(level_name)
	GameProgress.set_level_best_time(level_name, clear_time)
	if got_rings:
		GameProgress.set_level_rings_collected(level_name)
	# Unlock the next level
	var next_level : String = Levels.get_level_next(level_name)
	if next_level != "":
		GameProgress.set_level_unlocked(next_level)
	GameProgress.save_game()

func goto_next_level() -> void:
	# Fade out music if we've finished a group of four levels (because we'll change to a new track)
	if current_level in [3, 7]:
		MusicController.fade_out_music(1.0)
	# Transition out
	center_cutout_on_player()
	cutout.transition_out()
	yield(cutout, "transition_done")
	level_clear.hide()
	get_tree().paused = false
	if current_level < Levels.get_number_of_levels_in_set(current_set) - 1:
		current_level += 1
		init_game_section(Levels.get_level_in_set(current_set, current_level))
	else:
		init_game_section(Levels.get_epilogue_for_set(current_set))

func section_cleared() -> void:
	# Abrupt cutoff of music if we just cleared the set
	if current_level == 11:
		MusicController.stop_music()
	in_game = false
	get_tree().paused = true
	# Record progress
	var level_name : String = Levels.get_level_in_set(current_set, current_level)
	var clear_time : int = game_section.get_clear_time()
	var got_rings : bool = game_section.rings_collected >= 5
	var par_time : int = Levels.get_level_par(level_name)
	var dev_best : int = Levels.get_level_dev_best(level_name)
	var player_best : int = GameProgress.get_best_time_for_level(level_name)
	record_level_cleared(level_name, clear_time, got_rings)
	# Show level clear thing
	level_clear.set_level_stats(clear_time, par_time, dev_best, player_best, got_rings)
	level_clear.transition_in()
	level_clear.show()
	# Let the achievement handler know we've finished the level - maybe we just unlocked something?
	achievement_popup.level_finished(level_name, clear_time)

func pause() -> void:
	game_section.pause_timer()
	pause_screen.show_pause_menu()
	get_tree().paused = true
	paused = true

func resume() -> void:
	Input.action_release("jump")
	get_tree().paused = false
	game_section.resume_timer()
	paused = false

func back_to_title() -> void:
	in_game = false
	center_cutout_on_player()
	cutout.transition_out()
	yield(cutout, "transition_done")
	get_tree().change_scene("res://scenes/TitleScreen.tscn")

func start_conversation(conversation_name : String) -> void:
	game_section.pause_timer()
	get_tree().paused = true
	in_convo = true
	dialogue_window.play_conversation(conversation_name)

func finish_set() -> void:
	var next_set : String = Levels.get_next_set(current_set)
	if next_set != "":
		GameProgress.unlock_set(next_set)
	MusicController.fade_out_music(5.0)
	# Transition out - sloowly
	cutout.set_center(Vector2(0.5, 0.5))
	cutout.transition_out(3.0)
	yield(cutout, "transition_done")
	get_tree().change_scene("res://scenes/SetComplete.tscn")

func conversation_finished(conversation_name : String) -> void:
	GameProgress.set_conversation_had(conversation_name)
	achievement_popup.conversation_finished(conversation_name)
	# Was that the conversation that ends the set?
	if Conversations.conversation_has_flag(conversation_name, Conversations.FLAG_CONVO_EPILOGUE):
		finish_set()
	else:
		get_tree().paused = false
		game_section.resume_timer()
		in_convo = false

func _input(event : InputEvent) -> void:
	if in_game and not in_convo:
		if event.is_action_pressed("pause"):
			if not paused:
				pause()
		if event.is_action_pressed("restart"):
			get_tree().paused = true
			game_over()

func game_over() -> void:
	in_game = false
	center_cutout_on_player()
	cutout.transition_out()
	yield(cutout, "transition_done")
	level_clear.hide()
	restart_level()

func _on_LevelClear_goto_next_level() -> void:
	goto_next_level()

func _on_LevelClear_restart_level() -> void:
	game_over()

func _on_LevelClear_back_to_title() -> void:
	back_to_title()

func init_game_section(level_name : String) -> void:
	# If there's already a game section, get rid of it
	if game_section != null:
		game_section.queue_free()
	# Set everything up
	game_section = _GameSection.instance()
	game_section.level_data = Levels.get_level_data(level_name)
	add_child(game_section)
	game_section.connect("conversation_started", self, "start_conversation")
	game_section.connect("section_cleared", self, "section_cleared")
	game_section.connect("game_over", self, "game_over")
	game_section.set_bg_palette(Levels.get_bg_palette_for_level(level_name))
	game_section.set_bg_mountains(Levels.get_bg_mountains_for_level(level_name))
	# Cue the music
	var track_name : String = Levels.get_music_for_level(level_name)
	MusicController.play_music(track_name)
	# Do transition in
	center_cutout_on_player()
	cutout.transition_in()
	in_game = true
	game_section.start_timer()

func _ready() -> void:
	pause_screen.connect("resume", self, "resume")
	pause_screen.connect("restart_level", self, "game_over")
	pause_screen.connect("back_to_title", self, "back_to_title")
	dialogue_window.connect("conversation_finished", self, "conversation_finished")
	init_game_section(Levels.get_level_in_set(current_set, current_level))
