extends Control

const Icons : StreamTexture = preload("res://sprites/level_editor_icons.png")
const PlayerSprite : StreamTexture = preload("res://sprites/player.png")

const BESPOKE_OBJECT_MARKERS : Dictionary = {
	Constants.EDITOR_ITEM_ID_SPARK: preload("res://objects/level_editor/object_markers/SparkMarker.tscn"),
	Constants.EDITOR_ITEM_ID_SPIKE: preload("res://objects/level_editor/object_markers/SpikeMarker.tscn"),
	Constants.EDITOR_ITEM_ID_MOVING_PLATFORM: preload("res://objects/level_editor/object_markers/MovingPlatformMarker.tscn"),
	Constants.EDITOR_ITEM_ID_ORB: preload("res://objects/level_editor/object_markers/OrbMarker.tscn"),
	Constants.EDITOR_ITEM_ID_HAMMER: preload("res://objects/level_editor/object_markers/HammerMarker.tscn"),
	Constants.EDITOR_ITEM_ID_SPRINGBOARD_PLATFORM: preload("res://objects/level_editor/object_markers/SpringboardPlatformMarker.tscn"),
	Constants.EDITOR_ITEM_ID_TURRET: preload("res://objects/level_editor/object_markers/TurretMarker.tscn"),
	Constants.EDITOR_ITEM_ID_CARL: preload("res://objects/level_editor/object_markers/CarlMarker.tscn")
}
const _ObjectMarker : PackedScene = preload("res://objects/level_editor/object_markers/ObjectMarker.tscn")

const RoomLoader : GDScript = preload("res://scripts/RoomLoader.gd")
const RoomSaver : GDScript = preload("res://scripts/RoomSaver.gd")

const ROOM_SIZE : Vector2 = Vector2(42, 24)
const ROOM_RECT : Rect2 = Rect2(Vector2.ZERO, Vector2(42, 24) * Constants.TILE_SIZE)
const INNER_ROOM_RECT : Rect2 = Rect2(Vector2(1, 1) * Constants.TILE_SIZE, Vector2(40, 22) * Constants.TILE_SIZE)
const TILES : Array = [0, 1, 2]
const PLAYER_SPRITE_RECT : Rect2 = Rect2(0, 0, 48, 48)

onready var geometry : TileMap = $Geometry
onready var item_selector : Control = $ItemSelector
onready var label_item_hints : Label = $Label_ItemHints
onready var menu_button_file : MenuButton = $Panel_Navbar/HBox/MenuButton_File
onready var menu_button_options : MenuButton = $Panel_Navbar/HBox/MenuButton_Options
onready var file_dialog_load : FileDialog = $FileDialog_Load
onready var file_dialog_save : FileDialog = $FileDialog_Save

var current_tile : int = -1
var cursor_pos : Vector2 = Vector2(-1, -1)
var cursor_box_start_pos : Vector2 = Vector2(-1, -1)
var drawing_box : bool = false
var symmetry_h : bool = false
var symmetry_v : bool = false
var active : bool = true

func load_level() -> void:
	file_dialog_load.popup()
	update()

func save_level() -> void:
	file_dialog_save.popup()

func playtest() -> void:
	GameSession.playtesting = true
	var json : Dictionary = RoomSaver.save_room(geometry, geometry.player_spawn_pos)
	GameSession.playtest_data = json
	get_tree().change_scene("res://scenes/Testbed.tscn")

func update_menu_options() -> void:
	menu_button_options.get_popup().set_item_checked(0, symmetry_h)
	menu_button_options.get_popup().set_item_checked(1, symmetry_v)
	menu_button_options.get_popup().set_item_checked(3, geometry.goal_hidden_by_orbs)

func menu_file_button_pressed(id : int) -> void:
	match id:
		0: # new
			geometry.clear_level()
			update()
		1: # load
			load_level()
		2: # save
			save_level()

func menu_options_button_pressed(id : int) -> void:
	match id:
		10:
			symmetry_h = !symmetry_h
		11:
			symmetry_v = !symmetry_v
		12:
			geometry.goal_hidden_by_orbs = !geometry.goal_hidden_by_orbs
	update_menu_options()

func make_normalised_rect(start : Vector2, end : Vector2) -> Rect2:
	var result : Rect2 = Rect2(min(start.x, end.x), min(start.y, end.y), 0, 0)
	result.end = Vector2(max(start.x, end.x), max(start.y, end.y))
	return result

func get_mirror_h_pos(pos : Vector2) -> Vector2:
	pos.x = ROOM_SIZE.x - pos.x - 1
	return pos

func get_mirror_v_pos(pos : Vector2) -> Vector2:
	pos.y = ROOM_SIZE.y - pos.y - 1
	return pos

func get_mirrored_positions(pos : Vector2, mirror_h : bool, mirror_v : bool) -> Array:
	var results : Array = [pos]
	if mirror_h:
		results.append(get_mirror_h_pos(pos))
	if mirror_v:
		results.append(get_mirror_v_pos(pos))
		if mirror_h: # janky af
			results.append(get_mirror_h_pos(get_mirror_v_pos(pos)))
	return results

func _draw() -> void:
	draw_set_transform(geometry.position, 0, Vector2.ONE)
	# Draw player
	if geometry.player_spawn_pos != Vector2(-1, -1):
		var player_rect : Rect2 = Rect2((geometry.player_spawn_pos - Vector2(1, 2)) * Constants.TILE_SIZE, PLAYER_SPRITE_RECT.size)
		draw_texture_rect_region(PlayerSprite, player_rect, PLAYER_SPRITE_RECT)
	# Draw grid
	for x in range(0, ROOM_SIZE.x + 1):
		draw_line(Vector2(x, 0) * Constants.TILE_SIZE, Vector2(x, ROOM_SIZE.y) * Constants.TILE_SIZE, Color.darkgray)
	for y in range(0, ROOM_SIZE.y + 1):
		draw_line(Vector2(0, y) * Constants.TILE_SIZE, Vector2(ROOM_SIZE.x, y) * Constants.TILE_SIZE, Color.darkgray)
	draw_rect(INNER_ROOM_RECT, Color.white, false)
	# Draw bounding box
	if drawing_box:
		var bounding_box : Rect2 = make_normalised_rect(cursor_box_start_pos * Constants.TILE_SIZE, cursor_pos * Constants.TILE_SIZE)
		bounding_box.size += Constants.TILE_SIZE
		draw_rect(bounding_box, Color(1.0, 1.0, 1.0, 0.5))
	# Draw cursor
	if cursor_pos != Vector2(-1, -1):
		for position in get_mirrored_positions(cursor_pos, symmetry_h, symmetry_v):
			draw_rect(Rect2((position * Constants.TILE_SIZE) - Vector2.ONE, Constants.TILE_SIZE), Color.white, false, 3.0)

func change_tile(pos : Vector2, tile_type) -> void:
	for position in get_mirrored_positions(pos, symmetry_h, symmetry_v):
		geometry.set_cellv(position, tile_type)

func change_rect_of_tiles(start_pos : Vector2, end_pos : Vector2, tile_type : int) -> void:
	var rect : Rect2 = make_normalised_rect(start_pos, end_pos)
	for y in range(rect.position.y, rect.end.y + 1):
		for x in range(rect.position.x, rect.end.x + 1):
			change_tile(Vector2(x, y), tile_type)
	geometry.update_bitmask_region()
	update()

func place_object_marker(pos : Vector2, which_object : int) -> void:
	var object_marker : Node2D
	if which_object in BESPOKE_OBJECT_MARKERS:
		object_marker = BESPOKE_OBJECT_MARKERS[which_object].instance()
	else:
		object_marker = _ObjectMarker.instance()
		object_marker.which_object = which_object
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	geometry.add_child(object_marker)

func remove_object(pos : Vector2) -> void:
	for object in geometry.get_children():
		if object.board_position == pos:
			object.queue_free()
	label_item_hints.text = ""

func place_something(pos : Vector2, which_object : int) -> void:
	match which_object:
		Constants.EDITOR_ITEM_ID_DIRT, Constants.EDITOR_ITEM_ID_ICE, Constants.EDITOR_ITEM_ID_BRICK:
			change_tile(cursor_pos, which_object)
		Constants.EDITOR_ITEM_ID_PLAYER:
			remove_object(pos)
			geometry.player_spawn_pos = pos
		_: # object maker
			remove_object(pos)
			place_object_marker(pos, which_object)
	update_hints()
	update()

func remove_something(pos : Vector2, which_object : int) -> void:
	match which_object:
		Constants.EDITOR_ITEM_ID_PLAYER:
			if pos == geometry.player_spawn_pos:
				geometry.player_spawn_pos = Vector2(-1, -1)
		Constants.EDITOR_ITEM_ID_DIRT, Constants.EDITOR_ITEM_ID_ICE, Constants.EDITOR_ITEM_ID_BRICK:
			change_tile(cursor_pos, -1)
		_: # default = remove an object
			remove_object(pos)

func get_object_at_pos(pos : Vector2) -> Node2D:
	for object in geometry.get_children():
		if object.is_in_group("object_marker") and object.board_position == pos:
			return object
	return null

func update_hints() -> void:
	var object : Node2D = get_object_at_pos(cursor_pos)
	if object != null:
		label_item_hints.text = object.get_hints()
	else:
		label_item_hints.text = ""

func change_object_property(pos : Vector2) -> void:
	var object : Node2D = get_object_at_pos(pos)
	if object != null:
		object.change_property()
	update_hints()

func change_object_property_alt(pos : Vector2) -> void:
	var object : Node2D = get_object_at_pos(pos)
	if object != null:
		object.change_property_alt()
	update_hints()

func change_cursor_pos(new_cursor_pos : Vector2) -> void:
	cursor_pos = new_cursor_pos
	if not drawing_box:
		if Input.is_action_pressed("editor_place"):
			place_something(cursor_pos, item_selector.selected_object)
		elif Input.is_action_pressed("editor_remove"):
			remove_something(cursor_pos, item_selector.selected_object)
	seed(10)
	geometry.update_bitmask_region()
	# Change hints if needed
	update_hints()
	update()

func _input(event : InputEvent) -> void:
	if not active: return
	if event.is_action_pressed("editor_mirror_h"):
		symmetry_h = !symmetry_h
	elif event.is_action_pressed("editor_mirror_v"):
		symmetry_v = !symmetry_v
	elif event.is_action_pressed("editor_load"):
		load_level()
	elif event.is_action_pressed("editor_save"):
		save_level()
	elif event.is_action_pressed("editor_play"):
		playtest()
	update_menu_options()
	
func _process(delta : float) -> void:
	if not active: return
	var mouse_cursor : Vector2 = geometry.get_local_mouse_position()
	if ROOM_RECT.has_point(mouse_cursor):
		var new_cursor_pos : Vector2 = geometry.world_to_map(mouse_cursor)
		if cursor_pos != new_cursor_pos:
			change_cursor_pos(new_cursor_pos)
	# Check for user input
	if ROOM_RECT.has_point(mouse_cursor):
		if Input.is_action_pressed("editor_box"):
			if Input.is_action_just_pressed("editor_place"):
				cursor_box_start_pos = cursor_pos
				drawing_box = true
			elif Input.is_action_just_pressed("editor_remove"):
				cursor_box_start_pos = cursor_pos
				drawing_box = true
		else:
			if Input.is_action_just_pressed("editor_place"):
				place_something(cursor_pos, item_selector.selected_object)
			elif Input.is_action_just_pressed("editor_remove"):
				remove_something(cursor_pos, item_selector.selected_object)
			elif Input.is_action_just_pressed("editor_change_property"):
				change_object_property(cursor_pos)
			elif Input.is_action_just_pressed("editor_change_property_alt"):
				change_object_property_alt(cursor_pos)
	# One small detail: we should still let the user finish placing a box, even if the mouse has gone outside the tilemap
	if Input.is_action_pressed("editor_box"):
		if Input.is_action_just_released("editor_place"):
			drawing_box = false
			change_rect_of_tiles(cursor_box_start_pos, cursor_pos, item_selector.selected_object)
		elif Input.is_action_just_released("editor_remove"):
			drawing_box = false
			change_rect_of_tiles(cursor_box_start_pos, cursor_pos, -1)

func _ready() -> void:
	# Init the menu
	menu_button_file.get_popup().connect("id_pressed", self, "menu_file_button_pressed")
	menu_button_options.get_popup().connect("id_pressed", self, "menu_options_button_pressed")
	# If we're returning from a playtest session, load the level we just tested back into the editor
	if GameSession.playtesting:
		RoomLoader.load_room(GameSession.playtest_data, geometry)
		GameSession.playtesting = false

func _on_Button_Playtest_pressed():
	playtest()

func _on_FileDialog_Save_file_selected(path : String) -> void:
	var json : Dictionary = RoomSaver.save_room(geometry, geometry.player_spawn_pos)
	var file : File = File.new()
	file.open(path, File.WRITE)
	file.store_string(to_json(json))
	file.close()

func _on_FileDialog_Load_file_selected(path : String) -> void:
	var file : File = File.new()
	file.open(path, File.READ)
	var content : String = file.get_as_text()
	file.close()
	var json : Dictionary = parse_json(content)
	geometry.clear_level()
	RoomLoader.load_room(json, geometry)
	update_menu_options()

func _popup_about_to_show() -> void:
	active = false
	
func _popup_hide() -> void:
	active = true
